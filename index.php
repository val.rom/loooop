<?php
header('Content-Type:text/html;charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages=array();
    $errors = array();
    $values = array();
    if (!empty($_COOKIE['save'])) {
        setcookie('save','',100000);
        setcookie('login', '',100000);
        setcookie('pass', '', 100000);
        $messages['save'] = 'Спасибо, результаты сохранены.';
        if (!empty($_COOKIE['pass'])) {
            $messages['login_and_password'] = sprintf('<h1>Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
            и паролем <strong>%s</strong> для изменения данных.</h1>',
            strip_tags($_COOKIE['login']),
            strip_tags($_COOKIE['pass']));
          }
    }
    

    $errors['name'] = !empty($_COOKIE['name_error']);
    $errors['mail'] = !empty($_COOKIE['mail_error']);
    $errors['year'] = !empty($_COOKIE['year_error']);
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    $errors['check'] = !empty($_COOKIE['check_error']);
    $errors['biography'] = !empty($_COOKIE['biography_error']);


    if ($errors['name']) {
        if($_COOKIE['name_error']=='none'){
            setcookie('name_error','',100000);
            $messages['name'] = '<div class="error">Заполните имя.</div>';
    }
    if($_COOKIE['name_error']=='Unacceptable symbols'){
            setcookie('name_error','',100000);
            $messages['name'] = '<div class="error">Недопустимые символы в имени:а-я,А-Я,0-9,\ - _ </div>';
        }
    }
    if ($errors['mail']) {
        if($_COOKIE['mail_error']=='none'){
            setcookie('mail_error','',100000);
            $messages['mail'] = '<div class="error">Укажите почту.</div>';
        }
        if($_COOKIE['mail_error']=='invalid address'){
            setcookie('mail_error','',100000);
            $messages['mail'] = '<div class="error">Почта указана некорректно.Пример:login@mail.ru</div>';
        }
    }
    if($errors['year']){
        setcookie('year_error','',100000);
        $messages['year'] = '<div class="error">Укажите год рождения</div>';
    }
    if($errors['sex']){
            setcookie('sex_error','',100000);
            $messages['sex'] = '<div class="error">Укажите пол</div>';
    }
    if ($errors['limbs']) {
        setcookie('limbs_error','',100000);
        $messages['limbs'] = '<div class="error">Укажите количество конечностей</div>';
    }
    if ($errors['biography']) {
            setcookie('biography_error','',100000);
            $messages['biography'] = '<div class="error">Заполните биографию</div>';

    }
    if($errors['check']){
        setcookie('check_error','',100000);
        $messages['check'] = '<div class="error">нужно согласиться</div>';
    }
    $values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
    $values['mail'] = empty($_COOKIE['mail_value']) ? '' : strip_tags($_COOKIE['mail_value']);
    $values['year'] = empty($_COOKIE['year_value']) ? '' : strip_tags($_COOKIE['year_value']);
    $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
    $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : strip_tags($_COOKIE['limbs_value']);
    $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
    $values['check'] = empty($_COOKIE['check_value']) ? '' : strip_tags($_COOKIE['check_value']);


    if (session_start() && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
        $user = 'u20377';
        $password = '5006255';
        $login=$_SESSION['login'];
        $db = new PDO('mysql:host=localhost;dbname=u20377', $user, $password,
        array(PDO::ATTR_PERSISTENT => true));
        try{
        $stmt = $db->prepare("SELECT name, mail, birth, sex, limbs, bio FROM application WHERE login = '$login' ");
        $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_LAZY))
            {
                 $values['name']=$row['name'];
                 $values['mail']=$row['mail'];
                 $values['year']=$row['birth'];
                 $values['sex']=$row['sex'];
                 $values['limbs']=$row['limbs'];
                 $values['biography']=$row['biography'];
            }
    }catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
        } 
    }
    include('form.php');
}
else {
    /*Проверяем на ошибки*/
    $errors = FALSE;
            if (empty($_POST['name'])) {
                setcookie('name_error', '', time() + 24 * 60 * 60);
                setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else {
                if (!preg_match("#^[aA-zZ0-9\-_]+$#",$_POST['name'])){
                    setcookie('name_error', 'Неверные символы', time() + 24 * 60 * 60);
                    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
                    $errors=TRUE;
                }else{
                    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
                }
            }
            if (empty($_POST['mail'])) {
                setcookie('mail_error', '', time() + 24 * 60 * 60);
                setcookie('mail_value', $_POST['mail'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,10})$/i", $_POST['mail'])) {
                    setcookie('mail_error', 'Неверный адрес', time() + 24 * 60 * 60);
                    setcookie('mail_value', $_POST['mail'], time() + 30 * 24 * 60 * 60);
                    $errors = TRUE;
                }else{
                    setcookie('mail_value', $_POST['mail'], time() + 30 * 24 * 60 * 60);
                }
            }
            if (empty($_POST['year'])) {
                setcookie('year_error', 'none', time() + 24 * 60 * 60);
                setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('year_value', (int)$_POST['year'], time() + 30 * 24 * 60 * 60);
            }
            if (empty($_POST['sex'])) {
                setcookie('sex_error', 'none', time() + 24 * 60 * 60);
                setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
            }
            if (empty($_POST['limbs'])) {
                setcookie('limbs_error', 'none', time() + 24 * 60 * 60);
                setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
            }
            if (empty($_POST['biography'])) {
                setcookie('biography_error', 'none', time() + 24 * 60 * 60);
                setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
            }
            if (empty($_POST['check'])) {
                setcookie('check_error', 'none', time() + 24 * 60 * 60);
                setcookie('check_value', $_POST['check'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('check_value', $_POST['check'], time() + 30 * 24 * 60 * 60);
            }
            if ($errors) {
                header('Location:index.php');
                exit();
            }
            else {/*удаляем куки с ошибками*/
                setcookie('name_error', '', 100000);
                setcookie('mail_error', '', 100000);
                setcookie('year_error', '', 100000);
                setcookie('sex_error', '', 100000);
                setcookie('limbs_error', '', 100000);
                setcookie('biography_error', '', 100000);
                setcookie('check_error', '', 100000);
            }

        if (session_start() && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) 
        {//перезапись по логину
        $user = 'u20377';
        $password = '5006255';
        $login=$_SESSION['login'];
        $db = new PDO('mysql:host=localhost;dbname=u20377', $user, $password,array(PDO::ATTR_PERSISTENT => true));
        try {
        $stmt = $db->prepare("UPDATE application SET name=?, mail=?, birth=?, sex=?, limbs=?, bio=? WHERE login='$login'");
        
        $name=$_POST["name"];
        $mail=$_POST["mail"];
        $birth=$_POST["year"];
        $sex=$_POST["sex"];
        $limbs=$_POST["limbs"];
        $bio=$_POST["biography"];
        
        $stmt->execute(array($name,$mail,$birth,$sex,$limbs,$bio));
        }catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
    }else 
    {
        $lchar = '0123456789abcdefghijklmnopqrstuvwxyz';
        $pchar='0123456789abcdefghijklmnopqrstuvwxyz';
        $login = substr(str_shuffle($lchar), 0, 8);
        $password =substr(str_shuffle($pchar), 0, 8);
        // Сохраняем в Cookies.
        setcookie('login', $login);
        setcookie('pass', $password);
        
        $user = 'u20377';
        $pass = '5006255';
        $db = new PDO('mysql:host=localhost;dbname=u20377', $user, $pass,
        array(PDO::ATTR_PERSISTENT => true));
        try {
            $stmt = $db->prepare("INSERT INTO application (name, login, password, mail, birth, sex, limbs, bio) 
            VALUES (:name,:login,:password,:mail, :birth, :sex, :limbs, :bio)");
            $stmt->bindParam(':name', $name_db);
            $stmt->bindParam(':login', $login);
            $stmt->bindParam(':password', $pass_db);
            $stmt->bindParam(':mail', $mail_db);
            $stmt->bindParam(':birth', $year_db);
            $stmt->bindParam(':sex', $sex_db);
            $stmt->bindParam(':limbs', $limb_db);
            $stmt->bindParam(':bio', $bio_db);

            $name_db=$_POST["name"];
            $pass_db=md5($password);
            $mail_db=$_POST["mail"];
            $year_db=$_POST["year"];
            $sex_db=$_POST["sex"];
            $limb_db=$_POST["limbs"];
            $bio_db=$_POST["biography"];

            $stmt->execute();
        }
        catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
        }
    }
    setcookie('save', '1');
    header('Location:index.php');
}
?>
