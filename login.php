<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
session_start();
// Начинаем сессию.
if (!empty($_POST['exit'])) {
  session_destroy();
  session_write_close();
  header('Location:./');
}
// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.


// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  ?>
<html lang='ru'>
<head>
  <title>LOGIN</title>
  <link rel="stylesheet" href="mycss4.css" type="text/css"/>
</head>
<div class="Form">
  <form action="" method="post">
    Логин:<input name="login"/>
    <br>
    Пароль:<input name="pass" type="password"/>
    <br>
    <input type="submit" value="Войти" />
  </form>
</div>
<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
        $f=FALSE;
        $MESSAGE='';
        $user = 'u20377';
        $pass = '5006255';
        $log=$_POST['login'];
        $db = new PDO('mysql:host=localhost;dbname=u20377', $user, $pass,
        array(PDO::ATTR_PERSISTENT => true));
        if(!empty($_POST['login']) && !empty($_POST['pass'])){
           foreach($db->query("SELECT login, password FROM application ") as $row) {
              if($_POST['login']==$row['login'] && md5($_POST['pass'])==$row['password']){
                $f=TRUE;
                break;
              }
            }
            if($f){
              $_SESSION['login']=$_POST['login'];
              $_SESSION['user_id']=rand(1,255);
              header('Location:./');
            }else{
              print(md5($_POST['pass']));
              print("We dont know, who are you!");
            }
        }else{
          print("ВВедите логин/пароль!");
        }
    }       
?>
